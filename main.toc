\xpginauxfiletrue 
\selectlanguage *[babelshorthands=true]{russian}
\contentsline {chapter}{\numberline {1}Введение}{3}{chapter.1}%
\contentsline {chapter}{\numberline {2}Федеративная информационная платформа}{5}{chapter.2}%
\contentsline {section}{\numberline {2.1}Владение доменами}{6}{section.2.1}%
\contentsline {section}{\numberline {2.2}Инфраструктура данных}{7}{section.2.2}%
\contentsline {section}{\numberline {2.3}Данные как продукт}{8}{section.2.3}%
\contentsline {section}{\numberline {2.4}Федеративное управление вычислениями и хранением}{11}{section.2.4}%
\contentsline {chapter}{\numberline {3}Архитектура моделирования и обработки данных пакета технологий}{12}{chapter.3}%
\contentsline {section}{\numberline {3.1}Модели}{13}{section.3.1}%
\contentsline {section}{\numberline {3.2}Поддержка принятия решений}{15}{section.3.2}%
\contentsline {section}{\numberline {3.3}Оценка комплексной стоимости владения пакетом технологий}{16}{section.3.3}%
\contentsline {chapter}{\numberline {4}Заключение}{17}{chapter.4}%
\xpginauxfilefalse 
